/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#ifndef JuraSerial_h
#define JuraSerial_h

#include <cstddef>
#include <cstdint>

class JuraCodec
{
private:
	const uint8_t MASK = ~(1 << 2 | 1 << 5);
	static void bit_write(char& c, unsigned int x, bool v) {
		if (v)
			c = c | (1 << x);
		else
			c = c & ~(1 << x);
	}

	static bool bit_read(char c, unsigned int x) {
		return (c & (1 << x)) != 0;
	}

public:
	char encode_byte_part(char byte, uint8_t part) const {
		char res = 0xff;
		bit_write(res, 2, bit_read(byte, (2 * part) + 0));
		bit_write(res, 5, bit_read(byte, (2 * part) + 1));
		return res;
	}

	bool decode_byte_part(char &decoded, char inbyte, uint8_t part) const {
		bit_write(decoded, (2 * part) + 0, bit_read(inbyte, 2));
		bit_write(decoded, (2 * part) + 1, bit_read(inbyte, 5));
		return (MASK & inbyte) == MASK;
	}

	void encode(char* buffer, char input) const {
		for (uint8_t i = 0; i < 4; i++) {
			char b = encode_byte_part(input, i);
			buffer[i] = b;
		}
	}

	char decode(const char* buffer) const {
		char ret;
		for (uint8_t i = 0; i < 4; i++) {
			decode_byte_part(ret, buffer[i], i);
		}
		return ret;
	}
};

using RawDebugCallback = void (*)(char);

class IJuraSerial
{
protected:
	static void delay(unsigned int milliseconds);

public:
	virtual void begin() = 0;
	virtual int read() = 0;
	virtual size_t available() = 0;
	virtual size_t write(uint8_t byte) = 0;
	virtual size_t write(const char* c_str) = 0;
};

template <typename SerialAdapter>
class JuraSerial: public IJuraSerial
{
private:
	SerialAdapter& sa;
	RawDebugCallback dbg;

protected:
	JuraCodec codec;

public:
	JuraSerial(SerialAdapter& adapter, RawDebugCallback d = nullptr):
		sa(adapter), dbg(d) {};
	virtual ~JuraSerial() {};

	virtual void begin() override {
		sa.begin(9600);
	}

	virtual int read() override {
		int inb = sa.read();

		if (inb < 0)
			return inb;

		char outb = 0;

		for (uint8_t i = 0; i < 4; i++) {
			if (dbg)
				dbg(inb);
			if (!codec.decode_byte_part(outb, inb, i)) {
				return -1; // dont know what to do
			}
			if (i < 3) {
				while ((inb = sa.read()) < 0) {
					delay(0);
				};
			}
		}
		return outb;
	}

	virtual size_t available() override {
		return sa.available() / 4;
	}

	virtual size_t write(uint8_t byte) override {
		for (uint8_t i = 0; i < 4; i++) {
			char b = codec.encode_byte_part(byte, i);
			sa.write(&b, 1);
		}
		delay(8);
		return 1;
	}

	virtual size_t write(const char* c_str) override {
		size_t num = 0;
		char b;
		while ((b = *c_str) != '\0') {
			num += write(b);
			c_str++;
		}
		return num;
	}

};

#endif // JuraSerial_h

