/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#include "StringBuffer.h"
#include <string.h>
#include <stdint.h>

StringBuffer::StringBuffer(size_t length): length(length), index(0) {
	buffer = new char[length];
	buffer[0] = '\0';
}

StringBuffer::StringBuffer(const char* copy): StringBuffer(strlen(copy) + 1) {
	append(copy);
}

StringBuffer::StringBuffer(const StringBuffer& copy):
	StringBuffer(copy.length)
{
	index = copy.index;
	memcpy(buffer, copy.buffer, length);
}

StringBuffer::StringBuffer(StringBuffer&& rhs):
	length(rhs.length), buffer(rhs.buffer), index(rhs.index)
{
	rhs.buffer = nullptr;
	rhs.index = -1;
}

StringBuffer::~StringBuffer() {
	if (buffer)
		delete[] buffer;
}

char& StringBuffer::operator[](size_t index) {
	return buffer[index];
}

char StringBuffer::operator[](size_t index) const {
	return buffer[index];
}

bool StringBuffer::assign(const char* a) {
	clear();
	return append(a);
}

bool StringBuffer::put(char c) {
	if (index >= length) {
		return false;
	}
	buffer[index++] = c;
	buffer[index] = '\0';
	return true;
}

bool StringBuffer::append(const char* copy) {
	strncpy(&buffer[index], copy, length - index);
	relocateIndex();
	if (index > length) {
		return false;
	}
	return true;
}

bool StringBuffer::append(const StringBuffer& copy) {
	return append(copy.get());
}

void StringBuffer::appendHex(unsigned long long i, size_t length) {
	const char* hexLUT = "0123456789ABCDEF";
	for (int n = length - 1; n >= 0; n--) {
		uint8_t part = (i >> (n * 8)) & 0xff;
		uint8_t upper = part >> 4;
		uint8_t lower = part & 0xf;
		put(hexLUT[upper]);
		put(hexLUT[lower]);
	}
}

bool StringBuffer::isTerminated() const {
	return (buffer[index - 1] == '\n') && (buffer[index - 2] == '\r');
}

char* StringBuffer::get() {
	return buffer;
}

const char* StringBuffer::get() const {
	return buffer;
}

void StringBuffer::relocateIndex() {
	index = 0;
	while (buffer[index] != 0 && index <= length) {
		index++;
	}
}

void StringBuffer::clear() {
	index = 0;
	buffer[index] = '\0';
}

char StringBuffer::pop() {
	char c = buffer[--index];
	buffer[index] = '\0';
	return c;
}

