/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#include "JuraInterface.h"
#include <JuraSerial.h>
#include <JuraObfuscation.h>

#include <string.h>
#ifdef ESP8266
extern "C" char* strchrnul(const char*, int) noexcept(true);
#endif

JuraMessageView::JuraMessageView(const StringBuffer& buffer):
	buffer(buffer), obfuscationOn(false)
{
	init();
}

JuraMessageView::JuraMessageView(const char* buffer):
	buffer(buffer), obfuscationOn(false)
{
	init();
}

JuraMessageView::JuraMessageView(const StringBuffer& buffer, const JuraObfuscator& o):
	buffer(buffer)
{
	initDeobfuscate(o);
	init();
}

JuraMessageView::JuraMessageView(const char* buffer, const JuraObfuscator& o):
	buffer(buffer)
{
	initDeobfuscate(o);
	init();
}

void JuraMessageView::initDeobfuscate(const JuraObfuscator& obfuscator)
{
	obfuscationOn = (buffer[0] == '&');

	if (obfuscationOn) {
		obfuscator.deobfuscate((uint8_t*) buffer.get(), (uint8_t*) buffer.get() + 1);
	}
	// \0 will be placed in init()
}

void JuraMessageView::init()
{
	*strchrnul(buffer.get(), '\r') = '\0'; // strip newline

	// strip protocol type from command
	if (buffer[0] == '@') {
		commandStart = &buffer[1];
	} else {
		commandStart = buffer.get();
	}

	// handle parameters
	char* colonLoc = strchrnul(buffer.get(), ':');
	if (*colonLoc == '\0') {
		parameterStart = colonLoc;
		hasFirstPar = false;
	} else {
		*colonLoc = '\0';
		parameterStart = colonLoc + 1;
		hasFirstPar = true;
	}

	char* commaLoc = strchrnul(parameterStart, ',');
	if (*commaLoc == '\0') {
		secondParameterStart = commaLoc;
		hasSecondPar = false;
	} else {
		*commaLoc = '\0';
		secondParameterStart = commaLoc + 1;
		hasSecondPar = true;
	}
}

bool JuraMessageView::isNewProtocol() const
{
	return buffer[0] == '@';
}

bool JuraMessageView::hasFirstParameter() const
{
	return hasFirstPar;
}

bool JuraMessageView::hasSecondParameter() const
{
	return hasSecondPar;
}

bool JuraMessageView::isObfuscated() const
{
	return obfuscationOn;
}

const char* JuraMessageView::getCommand() const
{
	return commandStart;
}

const char* JuraMessageView::getFirstParameter() const
{
	return parameterStart;
}

const char* JuraMessageView::getSecondParameter() const
{
	return secondParameterStart;
}


JuraInterface::JuraInterface(IJuraSerial& serial, JuraObfuscator& obf):
	serial(serial), obfuscator(obf), buffer(257)
{}

bool JuraInterface::updateSerial()
{
	if (serial.available()) {
		while (serial.available()) {
			char ch = serial.read();
			buffer.put(ch);
			if (buffer.isTerminated()) {
				return true;
			}
		}
	}
	return false;
}

void JuraInterface::update()
{
	// TODO
	updateSerial();
}

void JuraInterface::sendPlain(const char* msg)
{
	serial.write(msg);
	serial.write("\r\n");
}

void JuraInterface::send(const char* message, bool obfuscated)
{
	if (obfuscated) {
		sendObfuscated(message);
	} else {
		sendPlain(message);
	}
}

void JuraInterface::sendObfuscated(const char* msg)
{
	const uint8_t key = 'n';
	StringBuffer in_buf(257);
	StringBuffer out_buf(257);
	in_buf.append(msg);
	in_buf.append("\r\n");
	in_buf.put('\0');
	out_buf[0] = '&';
	obfuscator.obfuscate((uint8_t*) out_buf.get() + 1, (const uint8_t*) in_buf.get(), key);
	out_buf.relocateIndex();
	out_buf.put('\0');
	sendPlain(out_buf.get());
}

bool JuraInterface::messageAvailable() const
{
	return buffer.isTerminated();
}

void JuraInterface::copyMessage(char* into, size_t num)
{
	strncpy(into, buffer.get(), num);
}

void JuraInterface::clearMessage()
{
	buffer.clear();
}

JuraMessageView JuraInterface::getMessage()
{
	JuraMessageView view(buffer, obfuscator);
	clearMessage();
	return view;
}

